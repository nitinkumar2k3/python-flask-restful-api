from usercode.user import User
import logging

def authenticate(username, password):
    user = User.find_by_username(username)
    logging.info("authenticate method call")
    if user and user.password == password:
        return user


def identity(payload):
    user_id = payload['identity']
    return User.find_by_id(user_id)
