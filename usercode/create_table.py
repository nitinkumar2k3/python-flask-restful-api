import sqlite3
"""for dummy table creation"""

connection = sqlite3.connect("data.db")
cursor = connection.cursor()

create_table = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username text, password text)"
cursor.execute(create_table)

create_table = "CREATE TABLE IF NOT EXISTS items (name text PRIMARY KEY, price real)"
cursor.execute(create_table)

user = (1, 'bob', 'asdf')
insert_query = "insert into users values(?,?,?)"
cursor.execute(insert_query, user)

moreusers= [
    (2,'nitin','kumar'),
    (3,'amit','kumar')
]
cursor.executemany(insert_query,moreusers)

select_query = "select * from users"
for row in cursor.execute(select_query):
    print(row)
# create_tabl   e = "CREATE TABLE IF NOT EXISTS items (name text PRIMARY KEY, price real)"
# cursor.execute(create_table)

connection.commit()

connection.close()
