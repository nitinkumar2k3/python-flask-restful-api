from flask import Flask
from flask_jwt import JWT
from flask_restful import Api

from usercode.item import Items, ItemList
from usercode.security import authenticate, identity
from usercode.user import UserRegister

app = Flask(__name__)
app.secret_key = 'jose'
api = Api(app)

jwt = JWT(app, authenticate, identity)  # /auth

api.add_resource(Items, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(UserRegister, '/register')
app.run(port=5000, debug='true')
